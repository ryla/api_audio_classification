def split_audio(audio_data, chunk_duration=4000):
    chunks = []

    for i in range(int(len(audio_data) / chunk_duration)):
        end = (i + 1) * chunk_duration
        if end <= len(audio_data):
            chunks.append(audio_data[i * chunk_duration:end])
