from feature_engineering import extract_features
import model
import os
import numpy as np

from flask import Flask, request, jsonify

app = Flask(__name__)
models = ['xgb', 'rf']
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# scaler = pickle.load(open(os.path.join(BASE_DIR, 'scalers/scaler.pkl'), 'rb'))


@app.route("/")
def hello():
    return "Welcome to our audio analysis API!"


@app.route('/predict', methods=['POST'])
def predict():
    data = request.json

    audio_data = np.array(data['audio_data'])
    sample_rate = data['sample_rate']
    n_labels = data['n_labels']
    type_features = data['type_features']
    proba = data['proba']

    features_1d, features_2d = extract_features(audio_data, sample_rate, type_features)
    predictions = {}

    for m in models:
        if m.startswith('cnn'):
            end_filename = '{0}_1s_mfcc_12_2d.sav'.format(m)
            features = features_2d
        else:
            end_filename = '{0}_1s_{1}.sav'.format(m, type_features)
            features = features_1d

        model_path = os.path.join(BASE_DIR, 'models/' + str(n_labels) + 'labels_model_' + end_filename)

        predictions[m] = model.predict(features, model_path, proba)

        '''if n_labels == 4 and not m.startswith('cnn'):
            basic_model_path = os.path.join(BASE_DIR, 'models/3labels_model_' + end_filename)
            negative_model_path = os.path.join(BASE_DIR, 'models/negative_model_' + end_filename)

            predictions[m + '_combined'] = model.combine_predictions(features,
                                                                     basic_model_path,
                                                                     negative_model_path,
                                                                     proba)

        elif n_labels == 5 and not m.startswith('cnn'):
            basic_model_path = os.path.join(BASE_DIR, 'models/3labels_model_' + end_filename)
            positive_model_path = os.path.join(BASE_DIR, 'models/positive_model_' + end_filename)
            negative_model_path = os.path.join(BASE_DIR, 'models/negative_model_' + end_filename)

            predictions[m + '_combined'] = model.combine_predictions(features,
                                                                     basic_model_path,
                                                                     negative_model_path,
                                                                     positive_model_path,
                                                                     proba)'''

    return jsonify(predictions)


if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port=5000)
