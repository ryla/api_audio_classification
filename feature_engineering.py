import numpy as np
import librosa


def extract_avg_mfcc_features(audio_data, sample_rate=22050, n_mfcc=12):
    """
    Extract MFCC
    :param audio_data: Array of floats, representing the amplitudes at different times
    :param sample_rate: The sampling rate of the audio
    :param n_mfcc: The number of MFC coefficients we want to have by sample
    :return: An array of length n_mfcc, containing floats
    """
    mfcc_features = np.mean(librosa.feature.mfcc(y=audio_data, sr=sample_rate, n_mfcc=n_mfcc), axis=1)
    return mfcc_features


def extract_chroma_features(audio_data, sample_rate=22050):
    stft = np.abs(librosa.stft(audio_data))
    chroma_features = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate), axis=1)
    return chroma_features


def extract_amplitude_features(audio_data, filter_amplitude=0.003):
    filtered_audio = audio_data[(audio_data >= filter_amplitude) | (audio_data <= -filter_amplitude)]
    signal2noise_ratio = len(filtered_audio) / len(audio_data)

    return [np.mean(audio_data), min(audio_data), max(audio_data), signal2noise_ratio]


def extract_features(audio_data, sample_rate=22050, type_features='mfcc_12'):
    """

    :param audio_data:
    :param sample_rate:
    :param type_features
    :return:
    """
    if type_features == 'mfcc_12':
        features_1d = extract_avg_mfcc_features(audio_data, sample_rate)
    elif type_features == 'chroma':
        features_1d = extract_chroma_features(audio_data, sample_rate)
    elif type_features == 'mfcc_12_and_chroma':
        features_1d = np.concatenate([extract_avg_mfcc_features(audio_data, sample_rate),
                                      extract_chroma_features(audio_data, sample_rate)])
    elif type_features == 'mfcc_12_and_chroma_and_amplitude':
        features_1d = np.concatenate([extract_avg_mfcc_features(audio_data, sample_rate),
                                      extract_chroma_features(audio_data, sample_rate),
                                      extract_amplitude_features(audio_data)])
    else:
        # TODO : Handle this error
        return

    features_2d = librosa.feature.mfcc(y=audio_data, sr=sample_rate)

    # TODO : add standardization : scaler.transform(features)
    return features_1d, features_2d
