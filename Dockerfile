FROM python:3.6
RUN mkdir /app
WORKDIR /app
ENV FLASK_APP main.py
COPY . /app
RUN apt-get -y update
RUN apt-get install -y libsndfile1
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 5000
CMD ["python", "main.py", "--host", "0.0.0.0"]
