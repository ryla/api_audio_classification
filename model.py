import numpy as np
import pickle
from keras import backend
from keras.models import load_model, Sequential


def load_my_model(model_path: str):
    """
    This function takes a path and loads a fitted classification model from it.
    If the model is from keras, it must be loaded using keras,
    while if it's an sklearn model, it can be loaded normally (using pickle).

    :param model_path: (str) The complete path of the file where the model is saved.
    :return: Either a sklearn.model_selection.GridSearchCV or a keras.models.Sequential object,
    which has already been fitted and can be used directly for classification.
    """

    model_name = model_path.split('/')[-1]
    if model_name.split('_')[2].startswith('cnn'):
        # If session isn't cleared, there can be an error raised
        backend.clear_session()
        model = load_model(model_path)
    else:
        model = pickle.load(open(model_path, 'rb'))
    return model


def get_prediction_from_proba(proba_prediction):
    """
    This function takes an array of probabilities, one for each class, and returns the
    most likely class.

    :param proba_prediction: One dimension array of probabilities (floats between 0.0 and 1.0)
    :return: (int) Most likely class
    """
    if len(proba_prediction) == 3 or len(proba_prediction) == 4:
        return np.argmax(proba_prediction) + 1
    elif len(proba_prediction) == 5:
        return np.argmax(proba_prediction)
    else:
        # TODO handle this error
        pass


def make_prediction(x, model, proba: bool = False):
    """

    :param x: Array representing the features extracted from one recording
    :param model: The predictive model we wish to use
    :param proba: If True, the function returns predicted probability for each class,
                  else returns most probable class
    :return: Depending on proba's value, either an array of probabilities, or a predicted class as integer
    """
    # This is the CNN case
    if isinstance(model, Sequential):
        x = np.reshape(x, (1, x.shape[0], 1))

        if proba:
            return model.predict([x])[0]
        else:
            return get_prediction_from_proba(model.predict([x])[0])

    else:
        if proba:
            return model.predict_proba([x])[0]
        else:
            return model.predict([x])[0]


def predict(x, model_path, proba: bool = False):
    """

    :param x: Array representing the features extracted from one recording
    :param model_path: The path where the machine learning model is saved
    :param proba: If True, the function returns predicted probability for each class,
                  else returns most probable class
    :return: Either a dictionary where each class has a probability, or one predicted class as string
    """
    # TODO : Check if model_path exists. If not, handle error

    model = load_my_model(model_path)
    prediction = make_prediction(x, model, proba)

    if proba:
        pred_dict = {}
        for i, p in enumerate(prediction):
            pred_dict[i] = str(p)
        return pred_dict
    else:
        return str(prediction)


def combine_predictions(x,
                        basic_model_path, negative_model_path, positive_model_path=None,
                        proba: bool = False):
    """

    :param x: Array representing the features extracted from one recording
    :param basic_model_path: The path where the 3-class (neutral/positive, negative, inaudible)
                             model is saved
    :param positive_model_path: The path where the binary (neutral vs positive) model is saved
    :param negative_model_path: The path where the binary (sad vs angry) model is saved
    :param proba: If True, the function returns predicted probability for each class,
                  else returns most probable class
    :return: Either a dictionary where each class has a probability, or one predicted class as string
    """
    # TODO : Figure out a way to make this work with probabilities

    basic_model = load_my_model(basic_model_path)
    prediction = make_prediction(x, basic_model, proba)

    if prediction == 3 or get_prediction_from_proba(prediction) == 3:
        negative_model = load_my_model(negative_model_path)
        prediction = make_prediction(x, negative_model, proba)
    elif positive_model_path is not None and (prediction == 1 or get_prediction_from_proba(prediction) == 1):
        positive_model = load_my_model(positive_model_path)
        prediction = make_prediction(x, positive_model, proba)

    if proba:
        pred_dict = {}
        for i, p in enumerate(prediction):
            pred_dict[i] = p
        return pred_dict
    else:
        return str(prediction)
